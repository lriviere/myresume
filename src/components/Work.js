import React, { Component } from 'react';
export default  class Work extends Component {
  render() {
    let resumeData = this.props.resumeData;
    return (
      <section id="work">

        <div className="row work" id="resume">
            <div className="three columns header-col">
               <h1><span>Work</span></h1>
            </div>

            <div className="nine columns main-col">
              {
                resumeData.work && resumeData.work.map((item,index) => {
                  return(
                    <div className="row item">
                       <div className="twelve columns">
                       <h3>{item.CompanyName}</h3>
                       <h6 className="location"><span><i class="fas fa-map-marker-alt"></i></span>{item.Location}</h6>
                          {
                            item.Details.map((i, index2)=>{
                              return(
                            <div className="info">
                              <li><em class="date">{i.startDate} / {i.endDate}</em><span>-</span>
                              {i.exp}</li>
                              <p>
                                {i.Achievements}
                              </p>
                              </div>
                                )
                              })
                            }
                       </div>
                    </div>
                  )
                })
              }
            </div> 
         </div>
</section>
    );
  }
}