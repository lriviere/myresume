import React, { Component } from 'react';
export default class ContactUs extends Component {
  render() {
    let resumeData = this.props.resumeData;
    return (
      <section id="contact">
          <div className="row contact-details">
            <div className="three columns header-col">
              <h1><span>Contact</span></h1>
            </div>

            <div className="nine columns main-col">
              <div className="row item">
                <div className="twelve columns">
                  <ul class="fa-ul mb-4 ml-0">
                  <li><span class="fa-li" ><i class="fa fa-envelope"></i></span>
                    <a href="mailto:contact@lriviere.me">
                      {resumeData.mail}</a>
                  </li>
                  <li><span class="fa-li" ><i class="fa fa-mobile"></i></span>{resumeData.phone}</li>
                  <li>
                  <span class="fa-li" >
                       <i class="fas fa-map-marker-alt"></i></span>{resumeData.address}
                  </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        );
  }
}