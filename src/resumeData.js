let resumeData = {
    "imagebaseurl":"https://rbhatia46.github.io/",
    "name": "Loona Rivière",
    "mail": "contact@lriviere.me",
    "phone": "06.16.45.69.91",
    "address" : "Aix-en-provence, France",
    "role": "Full Stack Developer",
    "linkedinId":"444767132",
    "skypeid": "Your skypeid",
    "roleDescription": "Work In Progress ...",
    "socialLinks":[
        {
          "name":"linkedin",
          "url":"https://www.linkedin.com/in/loona-riviere-444767132/",
          "className":"fab fa-linkedin"
        },
        {
          "name":"gitlab",
          "url":"https://gitlab.com/lriviere/",
          "className":"fab fa-gitlab"
        }
      ],
    "aboutme":"To be continued ...",
    "website":"https://gitlab.com/lriviere/",
    "work":[
      {
        "CompanyName":"Sogeti",
        "Details" : [
          {
            "exp":"Contrat de professionalisation",
            "startDate":"Octobre 2018",
            "endDate":"Septembre 2019",
            "Achievements":"Some Achievements"
          },
          {
            "exp":"Stage Applicatif Ingénieur",
            "startDate":"Juin 2018",
            "endDate":"Septembre 2018",
            "Achievements":"Some Achievements"
          }
        ],
        "Location": "Aix-en-provence, 13100 France"
      },
    ],
    "education":[
      {
        "UniversityName":"ISEN Toulon",
        "Details": [
          {
            "specialization":"Master BigData, Developpement Logiciel & Cloud Computing",
            "startDate":"2017",
            "endDate":"2019",
            "Achievements":"Some Achievements"
          },
          {
            "specialization":"Cycle Informatique et Réseaux",
            "startDate":"2014",
            "endDate":"2017",
            "Achievements":"Some Achievements"
          }
        ],
        "Location": "Toulon, 83000 France"
      },
      {
        "UniversityName":"Sherbrooke University",
        "Details": [
          {
            "specialization":"Génie Informatique",
            "startDate":"Avril 2017",
            "endDate":"Août 2017",
            "Achievements":"Some Achievements"
          }
        ],
        "Location": "Sherbrooke, Quebec"
      }
    ],
    "skillsDescription":"Your skills here",
    "skills":[
      {
        "skillname":"HTML5",
        "value": "90"
      },
      {
        "skillname":"CSS",
        "value": "85"
      },
      {
        "skillname":"Reactjs",
        "value": "40"
      },
      {
        "skillname":"Angular",
        "value": "80"
      },
      {
        "skillname":"Java",
        "value": "60"
      },
      {
        "skillname":"Docker",
        "value": "70"
      },
      {
        "skillname":"Git",
        "value": "70"
      }
    ],
    "portfolio":[
      {
        "name":"project1",
        "description":"mobileapp",
        "imgurl":"images/portfolio/phone.jpg"
      },
      {
        "name":"project2",
        "description":"mobileapp",
        "imgurl":"images/portfolio/project.jpg"
      },
      {
        "name":"project3",
        "description":"mobileapp",  
        "imgurl":"images/portfolio/project2.png"
      },
      {
        "name":"project4",
        "description":"mobileapp",
        "imgurl":"images/portfolio/phone.jpg"
      }
    ],
    "testimonials":[
      {
        "description":"This is a sample testimonial",
        "name":"Some technical guy"
      },
      {
        "description":"This is a sample testimonial",
        "name":"Some technical guy"
      }
    ]
  }
  
  export default resumeData